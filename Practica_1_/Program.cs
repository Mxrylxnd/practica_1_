﻿using System;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace Practica_1_
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ejercicio7();
            
        }

        public static void ejercicio1()
        {
            int numAprobados = 0;
            int numSobresalientes = 0;
            double nota;
            Console.WriteLine("Inserte 10 calificaciones");

            for (int i = 0; i < 10; i++)
            {
                nota = Double.Parse(Console.ReadLine());
                if (nota >= 9)
                {
                    numSobresalientes++;
                }
                else if (nota >= 5)
                {
                    numAprobados++;
                }
            }

            Console.WriteLine("El número de aprobados es: " + numAprobados);
            Console.WriteLine("El número de sobresalientes es " + numSobresalientes);
        }



        public static void ejercicio2()
        {
            string day, month;
            int mesnuevo, year;
            Console.WriteLine("inserte dia...");
            day = Console.ReadLine();
            Console.WriteLine("inserte mes...");
            month = Console.ReadLine();
            Console.WriteLine("inserte año...");
            year = Int32.Parse(Console.ReadLine());
            if (Int32.Parse(day) < 1 || Int32.Parse(day) > 30 || Int32.Parse(month) < 1 || Int32.Parse(month) > 12)
            {
                Console.WriteLine("No correcto");
            }
            else
            {
                mesnuevo = Int32.Parse(month) + 3;
                if (mesnuevo > 12)
                {
                    mesnuevo = mesnuevo - 12;
                    year++;
                }
                Console.WriteLine("la fecha " + day + "/" + mesnuevo + "/" + year);
            }
        }


        public static void ejercicio3()
        {

            int[] numeros = new int[10];
            int contador = numeros.Length - 1;

            numeros[0] = 8;
            numeros[1] = 5;
            numeros[2] = 6;
            numeros[3] = 7;
            numeros[4] = 8;
            numeros[5] = 9;
            numeros[6] = 10;
            numeros[7] = 11;
            numeros[8] = 12;
            numeros[9] = 13;


            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(numeros[i]);
            }


            while (contador >= 0)
            {
                Console.WriteLine(numeros[contador]);
                contador--;
            }
        }

        public static void ejercicio4()
        {
            int[] ingresosMes = new int[10];
            int[] gastosMes = new int[10];
            int resultado, resultadoFinal, numEmpresa;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Inserte ingresos al mes de la empresa " + (i + 1));
                ingresosMes[i] = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Inserte gastos al mes de la empresa " + (i + 1));
                gastosMes[i] = Int32.Parse(Console.ReadLine());
            }
            resultadoFinal = ingresosMes[0] - gastosMes[0];
            numEmpresa = 1;
            for (int i = 0; i < 10; i++)
            {
                resultado = ingresosMes[i] - gastosMes[i];
                if (resultado < resultadoFinal)
                {
                    resultadoFinal = resultado;
                    numEmpresa = (i + 1);
                }
            }
            Console.WriteLine("El peor resultado pertenece a la empresa " + numEmpresa + " con un resultado de: " + resultadoFinal);
        }

        public static void ejercicio5()
        {

            Console.WriteLine("Introduzca 10 números");
            int[] numeros = new int[10];
            int nMayor1 = 0;
            int nMayor2 = 0;
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine("Introduzca  número " + (i + 1));
                numeros[i] = Int32.Parse(Console.ReadLine());
            }
            for (int i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] > nMayor2)
                {
                    if (numeros[i] > nMayor1)
                    {
                        nMayor2 = nMayor1;
                        nMayor1 = numeros[i];
                    }
                    else
                    {
                        nMayor2 = numeros[i];
                    }
                }
            }
            Console.WriteLine("Los números con mayor valor son: " + nMayor1 + " y " + nMayor2);

        }

        public static void ejercicio7prueba()
        {
            int[] notas = new int[10];
            notas[0] = 8;
            notas[1] = 1;
            notas[2] = 2;
            notas[3] = 6;
            notas[4] = 3;
            notas[5] = 8;
            notas[6] = 9;
            notas[7] = 0;
            notas[8] = 9;
            notas[9] = 2;

            Array.Sort(notas);
            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine(notas[i]);
            }
        }

        public static void ejercicio7()
        {
            int[] notas = new int[10];
            int sumaMedias = 0;
            double media;
            Array.Sort(notas);
            Console.WriteLine("Introduzca diez calificaciones");
            for (int i = 0; i < notas.Length; i++)
            {
                notas[i] = Int32.Parse(Console.ReadLine());
            }
            if (notas[0] - notas[notas.Length-1] >=3)
            {
                for (int i = 1; i < notas.Length-1; i++)
                {
                    sumaMedias = sumaMedias + notas[i];
                }
                media = sumaMedias / 8;
                Console.WriteLine("La nota media es: " + media);
            }
            else
            {
                for (int i = 0; i < notas.Length; i++)
                {
                    sumaMedias = sumaMedias + notas[i];
                }
                media = sumaMedias / 10;
                Console.WriteLine("La nota media es: " + media);
            }
        }
       
            //comparar si notas[0] y notas[9] se diferencian 3 puntos
            //  si cumple - notas uso esos valores para hacer la media
            // si no cumple-uso esos valores`para hacer la media





    }
    }
